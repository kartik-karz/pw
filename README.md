
# Simple Command-Line Password Manager

This is a simple command-line password manager that keeps passwords
inside a **gpg** encrypted **tgz** archive. The content of the archive
is a directory tree with a file for each password entry.  The first
line of the file is the password, and the rest can optionally be
additional or related info. It provides commands for manipulating the
passwords, allowing the user to add, remove, edit, generate passwords
etc.

For more detailed documentation and examples see the man page:
https://dashohoxha.gitlab.io/pw/man/

It started by forking **[pass](http://www.passwordstore.org/)**.

Depends on: `bash`, `gnupg`, `git`, `xclip`, `pwgen`, `tree`,
`util-linux`, `ed`, `qrencode`, `imagemagick`

To install simply type `make`, or make a deb package with `deb.sh`.

To run all the tests try: `tests/run.sh`

For development and testing a docker container is recommneded:
https://gitlab.com/dashohoxha/pw/blob/master/ds/README.md
