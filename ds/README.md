# Container for developing and testing PW

## Installation

  - First install `ds`: https://github.com/docker-scripts/ds#installation

  - Then get the code from gitlab:
    ```
    git clone https://gitlab.com/dashohoxha/pw /opt/docker-scripts/pw
    ```

  - Create a directory for the container: `ds init pw/ds @pw-ds`

  - Fix the settings: `cd /var/ds/pw-ds/ ; vim settings.sh`

  - Copy the code of pw: `cp -a /opt/docker-scripts/pw /var/ds/pw-ds/`

  - Create the container: `ds make`


## Other commands

   ```
   ds stop
   ds start
   ds shell
   ds help

   ds jekyll
   ds user
   ds inject run-tests.sh
   ```
