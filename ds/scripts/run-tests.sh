#!/bin/bash -x

cd /host/pw/tests/
chown developer: -R .
sudo -h 127.0.0.1 -H -u developer ./run.sh "$@"
