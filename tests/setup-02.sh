source "$(dirname "$0")"/setup-01.sh

PASSPHRASE='123'
PASS1='01'
PASS2='02'
PASS3='03'

pwp() {
    pw "$@" <<<"$PASSPHRASE"
}

git config --global user.name 'test'
git config --global user.email 'test@example.org'
